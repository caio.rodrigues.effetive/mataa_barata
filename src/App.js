import React from 'react';
import Principal from './principal.jsx';
import './style.css';
import $ from 'jquery';
import Time from './Time.jsx';
import Fundo from './lab.jpg'; 
import Life from './Life.jsx';
function App() {

  return (
    <div id="app">
      <Time/>
      <Life/>
      <div id="render" className="render"><Principal/></div>
    </div>
  )
}

export default App;
$('body').ready(function () {
  let largura = window.innerWidth
  let altura = window.innerHeight
  $('#app').css({
    'width' : window.innerWidth,
    'height' : window.innerHeight,
    
    'background-image' : 'url('+ Fundo +')',
    'background-repeat': 'no-repeat',
    'background-size': largura + "px " + altura + "px"
  })
});