import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import App from './App';

class GamerOver extends Component {
    voltar = ()=> {
        <App />
    }
    render() {
        return (
            <div className="game-over">
                Game over
                <div><button onClick={()=>this.voltar()}>Jogar novamente</button></div>
            </div>
        );
    }
}

export default GamerOver;
