import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './style.css'
import Barata from './Barata.jsx';
import $ from 'jquery';
import CoracaoVazio from './coracao_vazio.png'
import GamerOver from './GamerOver.jsx';


class Dificil extends Component {
    iniciaJogoDificil = () =>{
        $('#time-div').show();
        $('#life-div').show();
        let core = 1;
        let min = 0;
        let seg = 0;
        let tempoPermitido = true;

        setInterval(() => {

            if(seg < 10){
                seg = '0'+ seg;
            }
            if(seg === 60){
                min++
                seg = 0
            }
            if(min >= 5){
                tempoPermitido = false;
            }

            let stringTime = '0'+ min+':' + seg;
            

            $('#time-div').html(stringTime);
            seg++;
        
        }, 1000);
        
       setInterval(() => {

        if(core < 3 && tempoPermitido){
            ReactDOM.render(
                <React.StrictMode>
                      <Barata />
                </React.StrictMode>,
                document.getElementById('render')
                );
                
                let left = Math.random() * (200 - (window.innerWidth - 200)) + (window.innerWidth - 200);
                let top =Math.random() * (150 - (window.innerHeight - 150)) + (window.innerHeight - 150);
    
                $('#barata').css({
                    'position' : 'relative',
                    'display': 'block',
                    'left' : left,
                    'top' : top
                })
    
                if($('#barata').attr('data-control-click') === 'true'){
                    
                    $('#barata').attr('data-control-click' , 'false');
                }else{
                    
                    $('#coracao-' + core).attr('src' , CoracaoVazio )
                    core++
    
                }
        }else{
            
            ReactDOM.render(
                <React.StrictMode>
                      <GamerOver />
                </React.StrictMode>,
                document.getElementById('app')
                );
             }

       }, 900);
    }
    render() {
        return (
            <div onClick={()=>this.iniciaJogoDificil()} className="dificuldade">
                Dificil
            </div>
        );
    }
}

export default Dificil;
