import React, { Component } from 'react';
import Barataimg from './barata-png.png';
import $ from 'jquery';

class Barata extends Component {

    clickBarata = () => {
        $('#barata').attr('data-control-click' , 'true')
        $('#barata').hide()
    }
    render() {
        return (
            <img onClick={() => this.clickBarata()} data-control-click="true" id="barata" className="barata" src={Barataimg} alt="" width="200px" height="150px"></img>
        );
    }
}

export default Barata;
 