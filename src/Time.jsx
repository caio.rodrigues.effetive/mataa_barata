import React, { Component } from 'react';
import './style.css';

class LifeTime extends Component {
    render() {
        return (
            <div id="time-div" className="time-div">
                 00:00
            </div>
        );
    }
}

export default LifeTime;
