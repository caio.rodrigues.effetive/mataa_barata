import React, { Component } from 'react';
import './style.css';
import Facil from './Facil.jsx';
import Medio from './Medio.jsx';
import Dificil from './Dificil.jsx';

class principal extends Component {

    render() {
        return (
            <div id="dificuldades-div" className="dificuldades-div">
                <Facil/>
                <Medio/>
                <Dificil/>
            </div>
        );
    }
}

export default principal;