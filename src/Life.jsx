import React, { Component } from 'react';
import CoracaoCheio from './coracao_cheio.png';

class Life extends Component {
    render() {
        return (
            <div id="life-div" className="life-div">
                <img src={CoracaoCheio} id="coracao-1" alt="" />
                <img src={CoracaoCheio} id="coracao-2" alt="" />
                <img src={CoracaoCheio} id="coracao-3" alt="" />
            </div>
        );
    }
}

export default Life;
